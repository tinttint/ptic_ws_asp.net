﻿// Decompiled with JetBrains decompiler
// Type: Proven.WebAPI.Common.HttpManager
// Assembly: Proven.WebAPI, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 93C303D1-90D7-44F5-9C6E-CAC582697C04
// Assembly location: C:\Users\jinny\Desktop\HO\bin\Proven.WebAPI.dll

using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;
using System.Text;

namespace Proven.WebAPI.Common
{
  public class HttpManager
  {
    public static object MakeRequest(string url, object param, HttpMethod method)
    {
      string requestUriString = "http://192.168.100.22/Factory/api/" + url;
      try
      {
        HttpWebRequest httpWebRequest = WebRequest.Create(requestUriString) as HttpWebRequest;
        if (param != null)
        {
          httpWebRequest.Method = "POST";
          httpWebRequest.ContentType = "application/json";
          byte[] bytes = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(param));
          Stream requestStream = httpWebRequest.GetRequestStream();
          byte[] buffer = bytes;
          int offset = 0;
          int length = bytes.Length;
          requestStream.Write(buffer, offset, length);
          requestStream.Close();
        }
        else
          httpWebRequest.Method = method == HttpMethod.POST ? "POST" : "GET";
        using (HttpWebResponse response = httpWebRequest.GetResponse() as HttpWebResponse)
        {
          if (response.StatusCode != HttpStatusCode.OK)
            throw new Exception(string.Format("Server error (HTTP {0}: {1}).", (object) response.StatusCode, (object) response.StatusDescription));
          return JsonConvert.DeserializeObject(new StreamReader(response.GetResponseStream()).ReadToEnd());
        }
      }
      catch (Exception ex)
      {
        Console.WriteLine(ex.Message);
        return (object) null;
      }
    }
  }
}
