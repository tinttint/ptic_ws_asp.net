﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Proven.WebAPI.Models;
using Proven.WebAPI.Service;
using Proven.WebAPI.EntityFramework;

namespace Proven.WebAPI.Controllers
{
    public class SalesInvoiceController : ApiController
    {
        [HttpPost]
        public async Task<IHttpActionResult> GetCountByObject(InvoiceParam param )
        {
            var result = await Task.Run(() => ServiceFactory.GetService<EntityFramework.Invoice, InvoiceParam>(ServiceType.SALESINVOICE).GetCountByObject(param));
            return Ok(result);
        }


        [HttpPost]
        public async Task<IHttpActionResult> SearchByObject(InvoiceParam param)
        {
            var result = await Task.Run(() => ServiceFactory.GetService<EntityFramework.Invoice, InvoiceParam>(ServiceType.SALESINVOICE).SearchByObject(param));
            return Ok(result);
        }

        public async Task<IHttpActionResult> Create(Invoice invoice)
        {
            HttpResponseMessage content = await Task.Run<HttpResponseMessage>((Func<HttpResponseMessage>)(() => ServiceFactory.GetService<Invoice, InvoiceParam>(ServiceType.SALESINVOICE).Create(invoice)));
            return content.StatusCode != HttpStatusCode.Created ? (IHttpActionResult)this.Content<string>(content.StatusCode, "") : (IHttpActionResult)this.Ok<HttpResponseMessage>(content);
        }
    }
}
