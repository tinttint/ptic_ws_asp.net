﻿// Decompiled with JetBrains decompiler
// Type: Proven.WebAPI.Controllers.FGRequestController
// Assembly: Proven.WebAPI, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 93C303D1-90D7-44F5-9C6E-CAC582697C04
// Assembly location: C:\Users\jinny\Desktop\HO\bin\Proven.WebAPI.dll

using Proven.WebAPI.EntityFramework;
using Proven.WebAPI.Models;
using Proven.WebAPI.Service;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Proven.WebAPI.Controllers
{
    public class FGRequestController : ApiController
    {
        private PTICEntities dbContext = WebApiApplication.GetDbContext();

        [HttpGet]
        public async Task<IHttpActionResult> Get()
        {
            var response = await Task.Run(() => ServiceFactory.GetService<FGRequest, FGRequestParam>(ServiceType.FGREQUSET).GetAll());
            return Ok(response);
        }

        [HttpPost]
        public async Task<IHttpActionResult> Create(FGRequest fgRequest)
        {
            HttpResponseMessage content = await Task.Run<HttpResponseMessage>((Func<HttpResponseMessage>)(() => ServiceFactory.GetService<FGRequest, FGRequestParam>(ServiceType.FGREQUSET).Create(fgRequest)));
            return content.StatusCode != HttpStatusCode.Created ? (IHttpActionResult)this.Content<string>(content.StatusCode, "") : (IHttpActionResult)this.Ok<HttpResponseMessage>(content);
        }

        [HttpPost]
        public async Task<IHttpActionResult> Delete(FGRequest fgRequest)
        {
            HttpResponseMessage content = await Task.Run<HttpResponseMessage>((Func<HttpResponseMessage>)(() => ServiceFactory.GetService<FGRequest, FGRequestParam>(ServiceType.FGREQUSET).Update(fgRequest)));
            return content.StatusCode != HttpStatusCode.Created ? (IHttpActionResult)this.Content<string>(content.StatusCode, "") : (IHttpActionResult)this.Ok<HttpResponseMessage>(content);
        }

        [HttpPost]
        public async Task<IHttpActionResult> StatusChange(FGRequestVO fgReq)
        {

            return (IHttpActionResult)this.Ok<HttpResponseMessage>(await Task.Run<HttpResponseMessage>((Func<HttpResponseMessage>)(() => ServiceFactory.GetService<FGRequest, FGRequestParam>(ServiceType.FGREQUSET).Update(fgReq.GetFGRequest()))));
        }

        [HttpPost]
        public async Task<IHttpActionResult> ConfirmRequest(FGRequest fgRequest)
        {
            HttpResponseMessage content = await Task.Run<HttpResponseMessage>((Func<HttpResponseMessage>)(() => ServiceFactory.GetService<FGRequest, FGRequestParam>(ServiceType.FGREQUSET).Update(fgRequest)));
            return content.StatusCode != HttpStatusCode.Created ? (IHttpActionResult)this.Content<string>(content.StatusCode, "") : (IHttpActionResult)this.Ok<HttpResponseMessage>(content);
        }
    }
}
