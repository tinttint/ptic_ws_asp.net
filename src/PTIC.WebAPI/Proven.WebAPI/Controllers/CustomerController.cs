﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Proven.WebAPI.Models;
using Proven.WebAPI.Service;

namespace Proven.WebAPI.Controllers
{
    public class CustomerController : ApiController
    {
        [HttpGet]
        public async Task<IHttpActionResult> Get()
        {
            var result = await Task.Run(() => ServiceFactory.GetService<EntityFramework.Customer,CustomerParam>(ServiceType.CUSTOMER).GetAll());
            return Ok(result);
        }
    }
}
