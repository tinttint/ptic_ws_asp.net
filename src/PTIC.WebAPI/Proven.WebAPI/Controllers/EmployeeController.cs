﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Proven.WebAPI.Models;
using Proven.WebAPI.Service;

namespace Proven.WebAPI.Controllers
{
    public class EmployeeController : ApiController
    {
        [HttpGet]
        public List<EntityFramework.Employee> Get()
        {
            return (List<EntityFramework.Employee>)ServiceFactory.GetService<EntityFramework.Employee, EmployeeParam>(ServiceType.EMPLOYEE).GetAll();
        }

        [HttpGet]
        public EntityFramework.Employee Get(int id)
        {
            return (EntityFramework.Employee)ServiceFactory.GetService<EntityFramework.Employee, EmployeeParam>(ServiceType.EMPLOYEE).GetById(id);
        }
    }
}
