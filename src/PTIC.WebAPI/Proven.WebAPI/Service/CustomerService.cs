﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using Proven.WebAPI.EntityFramework;
using Proven.WebAPI.Models;

namespace Proven.WebAPI.Service
{
    class CustomerService : IService<Customer, CustomerParam>
    {
        private static CustomerService instance = new CustomerService();

        Proven.WebAPI.EntityFramework.PTICEntities dbContext = WebApiApplication.GetDbContext();
        private CustomerService() { }

        public static CustomerService GetInstance()
        {
            return instance;
        }

        public HttpResponseMessage Create(Customer obj)
        {
            throw new NotImplementedException();
        }

        public HttpResponseMessage Delete(Customer obj)
        {
            throw new NotImplementedException();
        }

        public ICollection<Customer> GetAll()
        {
            return (from customer
                   in dbContext.Customers
                    where customer.IsDeleted == false select customer).ToList();
        }

        public Customer GetById(int id)
        {
            throw new NotImplementedException();
        }

        public List<ComboTemplateVO> GetCombo()
        {
            

            return (from customer
                    in dbContext.Customers
                    where customer.IsDeleted == false
                    select new ComboTemplateVO()
                    {
                        Value = customer.ID,
                        Display = customer.CusName
                    }).ToList();
        }

        public int GetCountByObject(CustomerParam param)
        {
            throw new NotImplementedException();
        }

        public ICollection<Customer> SearchByObject(CustomerParam param)
        {
            throw new NotImplementedException();
        }

        public HttpResponseMessage Update(Customer obj)
        {
            throw new NotImplementedException();
        }
    }

    public class CustomerParam { }
}