﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Proven.WebAPI.Models;

namespace Proven.WebAPI.Service
{
    public interface IComboService
    {
        List<ComboTemplateVO> GetCombo();
    }
}
