﻿using Proven.WebAPI.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Proven.WebAPI.Service
{
    public interface IFGRequestService
    {
        List<FGRequest> GetAllRequest();
        HttpResponseMessage AddRequest(FGRequest fgRequest);
        HttpResponseMessage DeleteRequest(int id);
    }
}
