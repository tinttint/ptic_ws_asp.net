﻿// Decompiled with JetBrains decompiler
// Type: Proven.WebAPI.Service.StockInWarehouseService
// Assembly: Proven.WebAPI, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 93C303D1-90D7-44F5-9C6E-CAC582697C04
// Assembly location: C:\Users\jinny\Desktop\HO\bin\Proven.WebAPI.dll

using Proven.WebAPI.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;

namespace Proven.WebAPI.Service
{
  public class StockInWarehouseService : IService<StockInWarehouse, StockInWarehouseService.StockInWarehouseParam>
  {
    private static StockInWarehouseService instance = new StockInWarehouseService();
    private PTICEntities dbcontext = WebApiApplication.GetDbContext();

    public StockInWarehouseService GetInstance()
    {
      return StockInWarehouseService.instance;
    }

    public HttpResponseMessage Create(StockInWarehouse obj)
    {
      throw new NotImplementedException();
    }

    public HttpResponseMessage Delete(StockInWarehouse obj)
    {
      throw new NotImplementedException();
    }

    public ICollection<StockInWarehouse> GetAll()
    {
      throw new NotImplementedException();
    }

    public StockInWarehouse GetFactoryById(int? id)
    {
      return this.dbcontext.StockInWarehouses.Where<StockInWarehouse>((Expression<Func<StockInWarehouse, bool>>) (stockInwarehouse => (int?) stockInwarehouse.ProductID == id & stockInwarehouse.WarehouseID == 1)).First<StockInWarehouse>();
    }

    public StockInWarehouse GetSSBById(int? id)
    {
      return this.dbcontext.StockInWarehouses.Where<StockInWarehouse>((Expression<Func<StockInWarehouse, bool>>) (stockInwarehouse => (int?) stockInwarehouse.ProductID == id & stockInwarehouse.WarehouseID == 2)).First<StockInWarehouse>();
    }

    public int GetCountByObject(StockInWarehouseService.StockInWarehouseParam param)
    {
      throw new NotImplementedException();
    }

    public int GetCountByObject(object param)
    {
      throw new NotImplementedException();
    }

    public ICollection<StockInWarehouse> SearchByObject(StockInWarehouseService.StockInWarehouseParam param)
    {
      throw new NotImplementedException();
    }

    public ICollection<StockInWarehouse> SearchByObject(object param)
    {
      throw new NotImplementedException();
    }

    public HttpResponseMessage Update(StockInWarehouse obj)
    {
      try
      {
        this.dbcontext.StockInWarehouses.Where<StockInWarehouse>((Expression<Func<StockInWarehouse, bool>>) (stockinwh => stockinwh.ID == obj.ID)).First<StockInWarehouse>();
        StockInWarehouse stockInWarehouse = obj;
        this.dbcontext.SaveChanges();
      }
      catch (Exception ex)
      {
        Console.WriteLine((object) ex);
      }
      return new HttpResponseMessage(HttpStatusCode.Created);
    }

    public StockInWarehouse GetById(int id)
    {
      throw new NotImplementedException();
    }

    public class StockInWarehouseParam
    {
    }
  }
}
