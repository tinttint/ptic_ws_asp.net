﻿using Proven.WebAPI.Common;
using Proven.WebAPI.EntityFramework;
using Proven.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;

namespace Proven.WebAPI.Service
{
    class SalesInvoiceService : IService<Invoice, InvoiceParam>
    {
        private static SalesInvoiceService instance = new SalesInvoiceService();

        EntityFramework.PTICEntities context = WebApiApplication.GetDbContext();

        public static SalesInvoiceService GetInstance()
        {
            return instance;
        }

        public ICollection<Invoice> GetAll()
        {

            var previousYear = DateTime.Now.AddYears(-1);
            return (from invoice in context.Invoices where invoice.IsDeleted == false && invoice.SalesDate >= previousYear select invoice).ToList();
        }

        public Invoice GetById(int id)
        {
            throw new NotImplementedException();
        }

        //public HttpResponseMessage Create(Invoice obj)
        //{
        //    try
        //    {
        //        this.context.Invoices.Add(obj);
        //        this.context.SaveChanges();

        //        DbSet<SalesDetail> saleDetail = this.context.SalesDetails;
        //        //for stock control
        //        foreach (SalesDetail sd in obj.SalesDetails)
        //        {
        //            StockInVehicle sv = (from stvehicle in context.StockInVehicles where stvehicle.ProductID == sd.ProductID && stvehicle.VehicleID == obj.VANID select stvehicle).FirstOrDefault();
        //            //this.context.StockInVehicles.Where<StockInVehicle>((Expression<Func<StockInVehicle, bool>>)(stvehicle => stvehicle.ProductID == sd.ProductID && stvehicle => stvehicle.VehicleID == obj.VANID.Value)).First<StockInVehicle>();
        //            sv.Qty -= sd.Qty;
        //            sv.LastModified = DateTime.Now;
        //        }

        //        //process for cash sale
        //        DbSet<Delivery> delivery = this.context.Deliverys;
        //        Invoice cashinv = this.context.Invoices.Where<Invoice>((Expression<Func<Invoice, bool>>)(invoice => invoice.CusID == obj.CusID)).First<Invoice>();
        //        cashinv.cashInvoice.DeliveryID = delivery.ID;
        //        cashinv.cashInvoice.SaleType = null;
        //        this.context.Invoices.Add(cashinv.cashInvoice);

        //        //for stock control
        //        foreach (SalesDetail sd in cashinv.cashInvoice.SalesDetails)
        //        {
        //            StockInVehicle siv = this.context.StockInVehicles.Where<StockInVehicle>((Expression<Func<StockInVehicle, bool>>)(stvehicle => stvehicle.ProductID == sd.ProductID, stvehicle => stvehicle.VehicleID == obj.VANID.Value)).First<StockInVehicle>();
        //            siv.Qty -= sd.Qty;
        //            siv.LastModified = cashinv.cashInvoice.SalesDate.Value;
        //        }
        //        delivery.Status = true;

        //        this.context.SaveChanges();
        //    }
        //    catch (Exception ex)
        //    {
        //        return new HttpResponseMessage(HttpStatusCode.Forbidden);
        //    }
        //    return new HttpResponseMessage(HttpStatusCode.Created);
        //}


        public HttpResponseMessage Create(Invoice newInvoice)
        {
            using (DbContextTransaction transaction = this.context.Database.BeginTransaction())
            {
                try
                {
                    //Insert a new Invoice
                    Invoice inv = new EntityFramework.Invoice();
                    inv.InvoiceNo = newInvoice.InvoiceNo;
                    inv.Status = newInvoice.Status;
                    inv.ManualInvoice = newInvoice.ManualInvoice;
                    inv.CusID = newInvoice.CusID;
                    inv.SalesPersonID = newInvoice.SalesPersonID;
                    inv.SalesDate = newInvoice.SalesDate;
                    inv.TotalAmt = newInvoice.TotalAmt;
                    inv.CommDiscAmt = newInvoice.CommDiscAmt;
                    inv.OtherAmt = newInvoice.OtherAmt;
                    inv.PaidAmt = (newInvoice.TotalAmt - newInvoice.CommDiscAmt) + newInvoice.OtherAmt;
                    inv.VoucherType = 1;
                    inv.Paid = true;
                    inv.TransportGateID = newInvoice.TransportGateID;
                    inv.Remark = newInvoice.Remark;
                    inv.DateAdded = DateTime.Now;
                    inv.IsDeleted = true;
                    if(newInvoice.WarehouseId != 0)
                    {
                        inv.WarehouseId = newInvoice.WarehouseId;
                    }
                    else
                    {
                        inv.WarehouseId = 0;
                    }
                    if(newInvoice.VANID != 0 || newInvoice.VANID != null)
                    {
                        inv.VANID = newInvoice.VANID;
                    }
                    else
                    {
                        inv.VANID = 0;
                    }
                   
                    inv.IsCancelled = false;

                    //Insert CashSales
                    foreach (SalesDetail sdRecord in newInvoice.SalesDetails)
                    {
                        inv.SalesDetails.Add(new EntityFramework.SalesDetail()
                        {
                            ProductID = sdRecord.ProductID,
                            SalePrice = sdRecord.SalePrice,
                            Qty = sdRecord.Qty,
                            Package = sdRecord.Package,
                            IsDeleted = false,
                            DateAdded = DateTime.Now
                        });
                    }

                    //save invoice
                    this.context.Invoices.Add(inv);

                    //stock control
                    foreach (SalesDetail sdRow in newInvoice.SalesDetails)
                    {
                        if (newInvoice.WarehouseId != 0)
                        {
                            //update stockinvehicle
                            StockInWarehouse siw = (from st in context.StockInWarehouses where st.ProductID == sdRow.ProductID && st.WarehouseID == newInvoice.WarehouseId select st).FirstOrDefault();
                            if (siw == null)
                            {
                                //insert into stockinwarehouse
                                StockInWarehouse siw1 = new EntityFramework.StockInWarehouse();
                                siw1.ProductID = sdRow.ProductID;
                                siw1.WarehouseID = newInvoice.WarehouseId;
                                siw1.Qty = -sdRow.Qty;
                                siw1.DateAdded = DateTime.Now;
                                siw1.IsDeleted = false;

                                //save
                                this.context.StockInWarehouses.Add(siw1);
                            }
                            else
                            {
                                siw.Qty = siw.Qty + (-sdRow.Qty);
                                siw.LastModified = DateTime.Now;
                                siw.IsDeleted = false;

                                //save
                                this.context.StockInWarehouses.Add(siw);
                            }

                            //insert into warehouseinout    
                            WarehouseInOut wio = new EntityFramework.WarehouseInOut();
                            wio.WarehouseID = newInvoice.WarehouseId;
                            wio.ProductID = sdRow.ProductID;
                            wio.StockBy = 3;
                            wio.Date = newInvoice.SalesDate.Value;
                            wio.Qty = -sdRow.Qty;
                            wio.Remark = null;
                            wio.DateAdded = DateTime.Now;
                            wio.IsDeleted = false;

                            //save
                            this.context.WarehouseInOuts.Add(wio);
                        }
                        if(newInvoice.VANID != 0 || newInvoice.VANID != null)
                        {
                            //update stockinvehicle
                            StockInVehicle siv = (from siv1 in context.StockInVehicles where siv1.ProductID == sdRow.ProductID select siv1).FirstOrDefault();
                            if (siv == null)
                            {
                                //insert into stockinvehicle
                                StockInVehicle siv1 = new EntityFramework.StockInVehicle();
                                siv1.VehicleID = newInvoice.VANID.Value;
                                siv1.ProductID = sdRow.ProductID;
                                siv1.Qty = -sdRow.Qty;
                                siv1.DateAdded = DateTime.Now;
                                siv1.IsDeleted = false;

                                //save
                                this.context.StockInVehicles.Add(siv1);
                            }
                            else
                            {
                                siv.Qty = siv.Qty + (-sdRow.Qty);
                                siv.LastModified = DateTime.Now;
                                siv.IsDeleted = false;

                                //save
                                this.context.StockInVehicles.Add(siv);
                            }

                            //insert into vehicleinout
                            VehicleInOut vio = new EntityFramework.VehicleInOut();
                            vio.VehicleID = newInvoice.VANID;
                            vio.ProductID = sdRow.ProductID;
                            vio.SalePersonID = sdRow.ProductID;
                            vio.StockBy = 3;
                            vio.Date = newInvoice.SalesDate.Value;
                            vio.Qty = (int)-sdRow.Qty;
                            vio.Remark = null;
                            vio.DateAdded = DateTime.Now;
                            vio.IsDeleted = false;

                            //save
                            this.context.VehicleInOuts.Add(vio);
                        }
                    }
                    

                    ////insert a new commision amount if passed
                   
                    //CommDiscount discount = new EntityFramework.CommDiscount();
                    //discount.InvoiceID = inv.ID;
                    //discount.SaleCommID = newInvoice.saleCommID;
                    //discount.CashCommID = newInvoice.cashCommID;
                    //discount.PackingAmt = newInvoice.packingAmt;
                    //discount.SaleCommAmt = newInvoice.saleCommAmt;
                   // discount.CashCommAmt = newInvoice.cashCommAmt;
                   // discount.OtherCommAmt = newInvoice.otherCommAmt;
                    //discount.RefundAmt = newInvoice.refundAmt;
                    //discount.NeedAmt = newInvoice.needAmt;
                    //discount.DateAdded = DateTime.Now;
                    //discount.IsDeleted = false;

                    ////save
                    //this.context.CommDiscounts.Add(discount);
                    

                    ////insert a new Tex if passed
                   
                    //Tax tax = new EntityFramework.Tax();
                    //tax.InvoiceID = inv.ID;
                   // tax.InsuranceAmt = newInvoice.insuranceAmt;
                    //tax.TaxAmt = newInvoice.taxAmt;
                   // tax.TransportAmt = newInvoice.transportAmt;
                    //tax.GateInvNo = newInvoice.gateInvNo;
                    //tax.GateInvPhoto = newInvoice.gateInvPhoto;
                    //tax.DateAdded = DateTime.Now;
                   // tax.IsDeleted = false;

                    ////save
                    //this.context.Taxes.Add(tax);
                   

                    //check 3 times per month customer
                    Customer customer = (from cus in context.Customers where cus.ID == newInvoice.ID select cus).FirstOrDefault();
                    if (customer != null)
                    {
                        DateTime startDate = newInvoice.SalesDate.Value.AddMonths(-6);
                        DateTime endDate = newInvoice.SalesDate.Value;
                        int result = (from invoice in context.Invoices
                                      where invoice.SalesDate >= startDate && invoice.SalesDate <= endDate && invoice.CusID == newInvoice.CusID
                                      select invoice).GroupBy(x => x.SalesDate.Value.ToString("MM")).Select(cl => cl.Count() > 0 ? 1 : 0).Sum();
                        if (result > 2)
                        {
                            Customer cus = (from cus1 in context.Customers where cus1.ID == newInvoice.CusID select cus1).FirstOrDefault();
                            if (cus != null)
                            {
                                cus.IsAlreadyParment = true;
                                cus.FirstParmanentDate = newInvoice.SalesDate.Value;
                                this.context.Customers.Add(cus);
                            }
                        }
                    }

                    this.context.SaveChanges();
                    
                    transaction.Commit();

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return new HttpResponseMessage(HttpStatusCode.Forbidden);
                }
            }
            return new HttpResponseMessage(HttpStatusCode.Created);            
        }

        public HttpResponseMessage Update(Invoice obj)
        {
            throw new NotImplementedException();
        }

        public HttpResponseMessage Delete(Invoice obj)
        {
            throw new NotImplementedException();
        }

        public ICollection<Invoice> SearchByObject(InvoiceParam param)
        {
            var result = (from invoice in context.Invoices
                          where invoice.IsDeleted == false
                          && (param.CustomerId.HasValue ? invoice.CusID == param.CustomerId.Value : true)
                          && (param.EmployeeId.HasValue ? invoice.SalesPersonID == param.EmployeeId.Value : true)
                          && (param.IsCancel.HasValue ? invoice.IsCancelled == param.IsCancel.Value : true)
                          && (param.FromDate.HasValue ? invoice.SalesDate >= param.FromDate.Value : true)
                          && (param.ToDate.HasValue ? invoice.SalesDate <= param.ToDate.Value : true)
                          orderby invoice.SalesDate descending
                          select invoice).Skip(param.PageIndex.Value * param.PageSize.Value).Take(param.PageSize.Value).ToList();
            return result;
        }

        public int GetCountByObject(InvoiceParam param)
        {
            return (from invoice in context.Invoices
                    where invoice.IsDeleted == false
                    && (param.CustomerId.HasValue ? invoice.CusID == param.CustomerId.Value : true)
                    && (param.EmployeeId.HasValue ? invoice.SalesPersonID == param.EmployeeId.Value : true)
                    && (param.IsCancel.HasValue ? invoice.IsCancelled == param.IsCancel.Value : true)
                    && (param.FromDate.HasValue ? invoice.SalesDate >= param.FromDate.Value : true)
                    && (param.ToDate.HasValue ? invoice.SalesDate <= param.ToDate.Value : true)
                    select invoice).Count();
        }

        private SalesInvoiceService() { }
    }

    public class InvoiceParam
    {
        public Nullable<DateTime> FromDate { get; set; }
        public Nullable<DateTime> ToDate { get; set; }
        public int? EmployeeId { get; set; }
        public int? CustomerId { get; set; }
        public bool? IsCancel { get; set; }
        public int? PageIndex { get; set; }
        public int? PageSize { get; set; }

    }
}