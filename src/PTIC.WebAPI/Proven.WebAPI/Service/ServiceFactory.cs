﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proven.WebAPI.Service
{
    public enum ServiceType { CUSTOMER,FGREQUSET,SALESINVOICE,EMPLOYEE}
    public static class ServiceFactory
    {
        public static IService<T,U> GetService<T,U>(ServiceType serviceType)
        {
            switch (serviceType)
            {
                case ServiceType.FGREQUSET: return (IService<T,U>) FGRequestService.GetInstance();
                case ServiceType.CUSTOMER: return (IService<T, U>)CustomerService.GetInstance();
                case ServiceType.SALESINVOICE: return (IService<T,U>)SalesInvoiceService.GetInstance();
                case ServiceType.EMPLOYEE: return (IService<T,U>)EmployeeService.GetInstance();
                default: throw new Exception("Invalid Service Type");
            }
        }
    }
}