﻿using Proven.WebAPI.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Proven.WebAPI.Service
{
    class WarehouseInOutService
    {
        PTICEntities dbcontext = WebApiApplication.GetDbContext();
        public HttpResponseMessage Create(WarehouseInOut w)
        {
            try
            {
                w.DateAdded = DateTime.Now;
                w.LastModified = DateTime.Now;
                w.Date = DateTime.Now;
                dbcontext.WarehouseInOuts.Add(w);
                dbcontext.SaveChanges();
            }catch(Exception e)
            {
                return new HttpResponseMessage(HttpStatusCode.Forbidden);
            }
            return new HttpResponseMessage(HttpStatusCode.Created);
        }
    }
}
