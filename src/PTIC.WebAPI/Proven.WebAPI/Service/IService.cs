﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace Proven.WebAPI.Service
{
    public interface IService<T, U>
    {
        int GetCountByObject(U param);
        ICollection<T> GetAll();
        T GetById(int id);
        HttpResponseMessage Create(T obj);
        HttpResponseMessage Update(T obj);
        HttpResponseMessage Delete(T obj);
        ICollection<T> SearchByObject(U param);

    }
}