﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using Proven.WebAPI.EntityFramework;
using Proven.WebAPI.Models;

namespace Proven.WebAPI.Service
{
    class EmployeeService : IService<EntityFramework.Employee,EmployeeParam>
    {

        private static EmployeeService instance = new EmployeeService();

        private EmployeeService() { }

        public static EmployeeService GetInstance()
        {
            return instance;
        }

        public List<ComboTemplateVO> GetCombo()
        {
            EntityFramework.PTICEntities dbContext = WebApiApplication.GetDbContext();
            return (from employee in dbContext.Employees where employee.IsActive == false select new ComboTemplateVO { Value = employee.ID, Display = employee.EmpName }).ToList();
        }

        public List<EntityFramework.Employee> GetAll()
        {
            EntityFramework.PTICEntities dbContext = WebApiApplication.GetDbContext();
            return (from employee in dbContext.Employees where employee.IsActive == false select employee).ToList();
        }

        public int GetCountByObject(EmployeeParam param)
        {
            throw new NotImplementedException();
        }

        ICollection<Employee> IService<Employee, EmployeeParam>.GetAll()
        {
            EntityFramework.PTICEntities dbContext = WebApiApplication.GetDbContext();
            return (from employee in dbContext.Employees where employee.IsActive == false select employee).ToList(); 
        }

        public Employee GetById(int id)
        {
            throw new NotImplementedException();
        }

        public HttpResponseMessage Create(Employee obj)
        {
            throw new NotImplementedException();
        }

        public HttpResponseMessage Update(Employee obj)
        {
            throw new NotImplementedException();
        }

        public HttpResponseMessage Delete(Employee obj)
        {
            throw new NotImplementedException();
        }

        public ICollection<Employee> SearchByObject(EmployeeParam param)
        {
            throw new NotImplementedException();
        }
    }
    public class EmployeeParam { }

}