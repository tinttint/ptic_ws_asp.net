﻿using Proven.WebAPI.Common;
using Proven.WebAPI.EntityFramework;
using Proven.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;

namespace Proven.WebAPI.Service
{
    internal class FGRequestService : IService<FGRequest, FGRequestParam>
    {
        private static FGRequestService instance = new FGRequestService();
        private PTICEntities dbContext = WebApiApplication.GetDbContext();

        private FGRequestService()
        {   
        }

        public static FGRequestService GetInstance()
        {
            return FGRequestService.instance;
        }

        public int GetCountByObject(FGRequestParam param)
        {
            throw new NotImplementedException();
        }

        //public HttpResponseMessage CreateCash()

        public ICollection<FGRequest> GetAll()
        {
            //return (ICollection<FGRequest>)this.dbContext.FGRequests.Where<FGRequest>((Expression<Func<FGRequest, bool>>)(fgRequest => fgRequest.ReqDate.HasValue && fgRequest.ReqDate.Value.Month == 5 && fgRequest.ReqDate.Value.Year == 2017)).ToList<FGRequest>();
            return (from fgRequest in dbContext.FGRequests where fgRequest.ReqDate.Value.Month == 5 && fgRequest.ReqDate.Value.Year == 2017 select fgRequest).ToList();
        }

        public FGRequest GetById(int id)
        {
            return this.dbContext.FGRequests.Where<FGRequest>((Expression<Func<FGRequest, bool>>)(fgRequest => fgRequest.ID == id)).FirstOrDefault<FGRequest>();
        }

        public HttpResponseMessage Create(FGRequest obj)
        {
            using (DbContextTransaction contextTransaction = this.dbContext.Database.BeginTransaction())
            {
                try
                {
                    obj.LastModified = new DateTime?(DateTime.Now);
                    obj.DateAdded = DateTime.Now;
                    this.dbContext.FGRequests.Add(obj);
                    this.dbContext.SaveChanges();
                    if (HttpManager.MakeRequest("FGRequest/HOCreate", (object)new FGRequestVO(obj)
                    {
                        DateAdded = new DateTime?(DateTime.Now),
                        LastModified = new DateTime?(DateTime.Now)
                    }, Proven.WebAPI.Common.HttpMethod.POST) == null)
                    {
                        contextTransaction.Rollback();
                        return new HttpResponseMessage(HttpStatusCode.GatewayTimeout);
                    }
                    contextTransaction.Commit();
                }
                catch (Exception ex)
                {
                    contextTransaction.Rollback();
                    return new HttpResponseMessage(HttpStatusCode.Forbidden);
                }
            }
            return new HttpResponseMessage(HttpStatusCode.Created);
        }

        public HttpResponseMessage Update(FGRequest obj)
        {
            using (DbContextTransaction contextTransaction = this.dbContext.Database.BeginTransaction())
            {
                try
                {
                    obj.LastModified = new DateTime?(DateTime.Now);
                    obj.DateAdded = DateTime.Now;
                    if (obj.Status == "Confirmed")
                        this.StockUpdate(obj);
                    this.dbContext.SaveChanges();
                    if (!obj.Status.Contains("Factory"))
                    {
                        if (HttpManager.MakeRequest("FGRequest/HOCreate", (object)new FGRequestVO(obj)
                        {
                            DateAdded = new DateTime?(DateTime.Now),
                            LastModified = new DateTime?(DateTime.Now)
                        }, Proven.WebAPI.Common.HttpMethod.POST) == null)
                        {
                            contextTransaction.Rollback();
                            return new HttpResponseMessage(HttpStatusCode.GatewayTimeout);
                        }

                    }
                    contextTransaction.Commit();
                }

                catch (Exception ex)
                {
                    contextTransaction.Rollback();
                    return new HttpResponseMessage(HttpStatusCode.Forbidden);
                }
                return new HttpResponseMessage(HttpStatusCode.Created);
            }
        }

        private void StockUpdate(FGRequest fgRequest)
        {
            StockInWarehouseService warehouseService = new StockInWarehouseService();
            FGRequest fg = this.dbContext.FGRequests.Where<FGRequest>((Expression<Func<FGRequest, bool>>)(fgrequest => fgrequest.ID == fgRequest.ID)).First<FGRequest>();
            DbSet<FGRequestDetail> fgRequestDetails = this.dbContext.FGRequestDetails;
            Expression<Func<FGRequestDetail, bool>> predicate = (Expression<Func<FGRequestDetail, bool>>)(fgreqdtl => fgreqdtl.FGReqID == (int?)fg.ID);
            foreach (FGRequestDetail fgRequestDetail in fgRequestDetails.Where<FGRequestDetail>(predicate).ToList<FGRequestDetail>())
            {
                if (fgRequestDetail.ProductID.HasValue)
                {
                    StockInWarehouse factoryById = warehouseService.GetFactoryById(fgRequestDetail.ProductID);
                    StockInWarehouse ssbById = warehouseService.GetSSBById(fgRequestDetail.ProductID);

                    StockInWarehouse stockInWarehouse1 = factoryById;
                    Decimal qty1 = factoryById.Qty;
                    Decimal? issueQty = fgRequestDetail.IssueQty;
                    Decimal num1 = issueQty.Value;
                    Decimal num2 = qty1 - num1;
                    stockInWarehouse1.Qty = num2;

                    StockInWarehouse stockInWarehouse2 = ssbById;
                    Decimal qty2 = ssbById.Qty;
                    issueQty = fgRequestDetail.IssueQty;
                    Decimal num3 = issueQty.Value;
                    Decimal num4 = qty2 + num3;
                    stockInWarehouse2.Qty = num4;

                    Decimal qtyIn = fgRequestDetail.IssueQty.Value;
                    Decimal qtyOut = (fgRequestDetail.IssueQty * -1).Value;
                    var wfactory = warehouseService.Update(factoryById);
                    if (wfactory.IsSuccessStatusCode)
                    {
                        WarehouseInOut wf = new WarehouseInOut()
                        {
                            ProductID = fgRequestDetail.ProductID.Value,
                            StockBy = 0,
                            Qty = qtyOut,
                            Remark = fgRequestDetail.FactoryRemark,
                            IsDeleted = false
                        };
                        WhouseInOut(wf);
                    }
                    var whouse = warehouseService.Update(ssbById);
                    if (whouse.IsSuccessStatusCode)
                    {
                        WarehouseInOut wf = new WarehouseInOut()
                        {
                            ProductID = fgRequestDetail.ProductID.Value,
                            StockBy = 1,
                            Qty = qtyIn,
                            Remark = fgRequestDetail.Remark,
                            IsDeleted = false
                        };
                        WhouseInOut(wf);
                    }
                }
            }
        }

        public void WhouseInOut(WarehouseInOut w)
        {
            WarehouseInOutService wh = new WarehouseInOutService();
            wh.Create(w);
        }

        public HttpResponseMessage Delete(FGRequest obj)
        {
            try
            {
                FGRequest fgRequest = this.dbContext.FGRequests.Where<FGRequest>((Expression<Func<FGRequest, bool>>)(fgrequest => fgrequest.ID == obj.ID)).First<FGRequest>();
                DbSet<FGRequestDetail> fgRequestDetails = this.dbContext.FGRequestDetails;
                Expression<Func<FGRequestDetail, bool>> predicate = (Expression<Func<FGRequestDetail, bool>>)(fgrequestdtl => fgrequestdtl.FGReqID == (int?)fgRequest.ID);
                foreach (FGRequestDetail entity in fgRequestDetails.Where<FGRequestDetail>(predicate).ToList<FGRequestDetail>())
                    this.dbContext.FGRequestDetails.Remove(entity);
                this.dbContext.FGRequests.Remove(fgRequest);
                this.dbContext.SaveChanges();
            }
            catch (Exception ex)
            {
                Console.WriteLine((object)ex);
            }
            return new HttpResponseMessage(HttpStatusCode.Created);
        }

        public ICollection<FGRequest> SearchByObject(FGRequestParam param)
        {
            throw new NotImplementedException();
        }

       
    }
    public class FGRequestParam
    {
    }

}
