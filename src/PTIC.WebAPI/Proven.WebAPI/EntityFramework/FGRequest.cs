//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Proven.WebAPI.EntityFramework
{
    using System;
    using System.Collections.Generic;
    
    public partial class FGRequest
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public FGRequest()
        {
            this.FGRequestDetails = new HashSet<FGRequestDetail>();
        }
    
        public int ID { get; set; }
        public string ReqVouNo { get; set; }
        public Nullable<System.DateTime> ReqDate { get; set; }
        public Nullable<System.DateTime> RequireDate { get; set; }
        public Nullable<System.DateTime> IssueDate { get; set; }
        public Nullable<int> RequesterID { get; set; }
        public Nullable<int> TransportVenID { get; set; }
        public Nullable<int> TarnsportEmpID { get; set; }
        public string Remark { get; set; }
        public string FactoryFormRemark { get; set; }
        public System.DateTime DateAdded { get; set; }
        public Nullable<System.DateTime> LastModified { get; set; }
        public bool IsDeleted { get; set; }
        public string IssueVouNo { get; set; }
        public bool IsExported { get; set; }
        public Nullable<System.DateTime> ExportedTime { get; set; }
        public string VehicleNo { get; set; }
        public string TransportorName { get; set; }
        public Nullable<byte> CancelFlag { get; set; }
        public Nullable<bool> CancelFlags { get; set; }
        public string Status { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FGRequestDetail> FGRequestDetails { get; set; }
    }
}
