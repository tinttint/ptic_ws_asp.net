﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Routing;

namespace Proven.WebAPI
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        private static EntityFramework.PTICEntities context = new EntityFramework.PTICEntities();

        public static EntityFramework.PTICEntities GetDbContext()
        {
            return context;
        }
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
        }

 
    }
}
