﻿// Decompiled with JetBrains decompiler
// Type: Proven.WebAPI.Models.FGRequestDetailVO
// Assembly: Proven.WebAPI, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 93C303D1-90D7-44F5-9C6E-CAC582697C04
// Assembly location: C:\Users\jinny\Desktop\HO\bin\Proven.WebAPI.dll

using Proven.WebAPI.EntityFramework;
using System;

namespace Proven.WebAPI.Models
{
  public class FGRequestDetailVO
  {
    public int? ID { get; set; }

    public int? FGReqInfoID { get; set; }

    public int? ProductID { get; set; }

    public Decimal? RequsetQty { get; set; }

    public Decimal? IssueQty { get; set; }

    public string HORemark { get; set; }

    public string FactoryRemark { get; set; }

    public bool IsDeleted { get; set; }

    public DateTime? DateAdded { get; set; }

    public DateTime? LastModified { get; set; }

    public FGRequestDetailVO()
    {
    }

    public FGRequestDetailVO(FGRequestDetail fgdtl)
    {
      this.ID = new int?(fgdtl.ID);
      this.FGReqInfoID = fgdtl.FGReqID;
      this.ProductID = fgdtl.ProductID;
      this.RequsetQty = fgdtl.Qty;
      this.IssueQty = fgdtl.IssueQty;
      this.HORemark = fgdtl.Remark;
      this.FactoryRemark = fgdtl.FactoryRemark;
      this.DateAdded = new DateTime?(fgdtl.DateAdded);
      this.LastModified = fgdtl.LastModified;
      this.IsDeleted = fgdtl.IsDeleted;
    }
  }
}
