﻿// Decompiled with JetBrains decompiler
// Type: Proven.WebAPI.Models.FGRequestVO
// Assembly: Proven.WebAPI, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 93C303D1-90D7-44F5-9C6E-CAC582697C04
// Assembly location: C:\Users\jinny\Desktop\HO\bin\Proven.WebAPI.dll

using Proven.WebAPI.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Proven.WebAPI.Models
{
  public class FGRequestVO
  {
    private PTICEntities dbcontext = WebApiApplication.GetDbContext();

    public int? ID { get; set; }

    public string RequestNo { get; set; }

    public string IssueNo { get; set; }

    public DateTime? IssueDate { get; set; }

    public DateTime? ReqDate { get; set; }

    public DateTime? RequireDate { get; set; }

    public string TransportVenNo { get; set; }

    public string HORemark { get; set; }

    public string FactoryRemark { get; set; }

    public string RequestPersonName { get; set; }

    public string Status { get; set; }

    public DateTime? DateAdded { get; set; }

    public DateTime? LastModified { get; set; }

    public virtual ICollection<FGRequestDetailVO> FGRequestDetails { get; set; }

    public FGRequestVO()
    {
    }

    public FGRequestVO(FGRequest fgRequest)
    {
      this.ID = new int?(fgRequest.ID);
      this.RequestNo = fgRequest.ReqVouNo;
      this.ReqDate = fgRequest.ReqDate;
      this.RequireDate = fgRequest.RequireDate;
      this.HORemark = fgRequest.Remark;
      this.FactoryRemark = fgRequest.FactoryFormRemark;
      this.RequestPersonName = this.dbcontext.Employees.Where<Employee>((Expression<Func<Employee, bool>>) (employee => (int?) employee.ID == fgRequest.RequesterID)).Select<Employee, string>((Expression<Func<Employee, string>>) (employee => employee.EmpName)).FirstOrDefault<string>();
      this.Status = fgRequest.Status;
      this.LastModified = fgRequest.LastModified;
      this.DateAdded = new DateTime?(fgRequest.DateAdded);
      this.TransportVenNo = fgRequest.VehicleNo;
      this.IssueDate = fgRequest.IssueDate;
      this.IssueNo = fgRequest.IssueVouNo;
      this.FGRequestDetails = (ICollection<FGRequestDetailVO>) new List<FGRequestDetailVO>();
      foreach (FGRequestDetail fgRequestDetail in (IEnumerable<FGRequestDetail>) fgRequest.FGRequestDetails)
        this.FGRequestDetails.Add(new FGRequestDetailVO()
        {
          ID = new int?(fgRequestDetail.ID),
          FGReqInfoID = fgRequestDetail.FGReqID,
          ProductID = fgRequestDetail.ProductID,
          RequsetQty = fgRequestDetail.Qty,
          IssueQty = fgRequestDetail.IssueQty,
          HORemark = fgRequestDetail.Remark,
          FactoryRemark = fgRequestDetail.FactoryRemark,
          DateAdded = new DateTime?(fgRequestDetail.DateAdded),
          LastModified = fgRequestDetail.LastModified
        });
    }

        public  FGRequest GetFGRequest()
        {
            FGRequest fgRequest = (from fgreq in WebApiApplication.GetDbContext().FGRequests where fgreq.ID == this.ID select fgreq).FirstOrDefault();
            if (fgRequest != null)
            {
                fgRequest.ReqVouNo = this.RequestNo;
                fgRequest.ReqDate = this.ReqDate;
                fgRequest.RequireDate = this.RequireDate;
                fgRequest.Remark = this.HORemark;
                fgRequest.FactoryFormRemark = this.FactoryRemark;
                fgRequest.Status = this.Status;
                fgRequest.LastModified = this.LastModified;
                fgRequest.VehicleNo = this.TransportVenNo;
                fgRequest.IssueDate = this.IssueDate;
                fgRequest.IssueVouNo = this.IssueNo;


                foreach (FGRequestDetail fgRequestDetail in fgRequest.FGRequestDetails)
                {
                     FGRequestDetailVO dtlvo = this.FGRequestDetails.Single(x => x.FGReqInfoID == fgRequest.ID && x.ProductID == fgRequestDetail.ProductID);
                    if (dtlvo != null)
                    {
                        fgRequestDetail.IssueQty = dtlvo.IssueQty;
                        fgRequestDetail.Remark = dtlvo.HORemark;
                        fgRequestDetail.FactoryRemark = dtlvo.FactoryRemark;
                        fgRequestDetail.LastModified = dtlvo.LastModified;

                    }

                }
            }
            return fgRequest;
        }
    }
}
