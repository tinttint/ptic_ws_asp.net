﻿// Decompiled with JetBrains decompiler
// Type: Proven.WebAPI.Models.ComboTemplateVO
// Assembly: Proven.WebAPI, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 93C303D1-90D7-44F5-9C6E-CAC582697C04
// Assembly location: C:\Users\jinny\Desktop\HO\bin\Proven.WebAPI.dll

namespace Proven.WebAPI.Models
{
  public class ComboTemplateVO
  {
    public int Value { get; set; }

    public string Display { get; set; }
  }
}
