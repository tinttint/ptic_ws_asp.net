﻿using System;
using Proven.WebAPI.EntityFramework;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proven.WebAPI.Models
{
    public class InvoiceVO
    {
        public int invoiceID { get; set; }

        public int deliveryID { get; set; }

        public int cusID { get; set; }

        public int salePersonID { get; set; }

        public int transportTypeID { get; set; }

        public int transportGateID { get; set; }

        public string invoiceNo { get; set; }

        public int saleType { get; set; }

        public DateTime saleDate { get; set; }

        public string manualInvoice { get; set; }

        public string status { get; set; }

        public Decimal totalAmt { get; set; }

        public Decimal commDisAmt { get; set; }

        public Decimal otherAmt { get; set; }

        public Decimal paidAmt { get; set; }

        public int voucherType { get; set; }

        public bool paid { get; set; }

        public string remark { get; set; }

        public DateTime DateAdded { get; set; }

        public DateTime LastModified { get; set; }

        public bool isDeleted { get; set; }

        public virtual ICollection<SalesDetailVO> saleDetail { get; set; }

        public InvoiceVO()
        {

        }
        
    }
}