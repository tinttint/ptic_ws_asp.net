﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proven.WebAPI.Models
{
    public class SalesDetailVO
    {
        public int? ID { get; set; }

        public int? InvoiceID { get; set; }

        public int? ProductID { get; set; }

        public Decimal? SalePrice { get; set; }

        public Decimal? Qty { get; set; }

        public int? Package { get; set; }

        public Decimal Amount { get; set; }

        public DateTime DateAdded { get; set; }

        public DateTime LastModified { get; set; }

        public bool IsDeleted { get; set; }
    }
}